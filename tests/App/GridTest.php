<?php

use PHPUnit\Framework\TestCase;
use Kata\App\Grid;

class GridTest extends TestCase
{

    /** @test */
    public function Given_a_vertical_limit_When_get_the_limit_Then_rerturns_original_limit()
    {
        //Given
        $verticalLimit = 10;
        $grid = new Grid();
        $grid->setVerticalLimit($verticalLimit);

        //When
        $response  = $grid->getVerticalLimit();

        //Then
        $this->assertSame($verticalLimit, $response);
    }

    /** @test */
    public function Given_a_horizontal_limit_When_get_the_limit_Then_rerturns_original_limit()
    {
        //Given
        $horizontalLimit = -10;
        $grid = new Grid();
        $grid->setHorizontalLimit($horizontalLimit);

        //When
        $response  = $grid->getHorizontalLimit();

        //Then
        $this->assertSame($horizontalLimit, $response);
    }

    /** @test */
    public function Given_not_a_horizontal_limit_When_get_the_limit_Then_returns_minimal_limit()
    {
        $grid = new Grid();

        $response  = $grid->getHorizontalLimit();

        $this->assertSame(Grid::minimalLimit,$response);
    }

}