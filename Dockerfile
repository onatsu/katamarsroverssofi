FROM php:7.4

WORKDIR /app

ENV APP_ENV dev

RUN  apt-get update \
  && apt-get install -y \
        libzip-dev \
        zip \
        vim \
        curl \
  && apt-get clean \
  && pecl install xdebug-3.0.4 \
  && docker-php-ext-enable xdebug \
  && docker-php-ext-install zip \
  && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
