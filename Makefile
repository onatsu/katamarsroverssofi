.PHONY: build
build:	## build local environment
	docker build -t marsrover:php .
	docker run --rm -v $$PWD:/app -v $$HOME/.composer:/tmp -u $$(id -u):$$(id -g) marsrover:php composer install

.PHONY: tests
tests: build	## run unit tests
	docker run --rm -v $$PWD:/app --user $$(id -u):$$(id -g) marsrover:php /app/vendor/bin/phpunit -vv --debug

.PHONY: help
help: ## Display this help message
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'