<?php

namespace Kata\App;

class Grid
{

    const minimalLimit = 10;
    private $verticalLimit = self::minimalLimit;
    private $horizontalLimit = self::minimalLimit;

    /**
     * @return mixed
     */
    public function getHorizontalLimit()
    {
        return $this->horizontalLimit;
    }

    /**
     * @param mixed $horizontalLimit
     */
    public function setHorizontalLimit($horizontalLimit): void
    {
        $this->horizontalLimit = $horizontalLimit;
    }

    /**
     * Grid constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getVerticalLimit()
    {
        return $this->verticalLimit;
    }

    /**
     * @param mixed $verticalLimit
     */
    public function setVerticalLimit(int $verticalLimit): void
    {
        $this->verticalLimit = $verticalLimit;
    }



}